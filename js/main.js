function equalHeight(group) {
  tallest = 0;
  group.each(function() {
    thisHeight = $(this).height();
    if(thisHeight > tallest) {
      tallest = thisHeight;
    }
  });
  group.height(tallest);
}

$(document).ready(function() {

  // page is now ready, initialize the calendar...

  $('#calendar').fullCalendar({
		height: 'auto',
    header: {
	    left:   'prev',
	    center: 'title',
	    right:  'next',
		},
  });

  $('.video-list').on('click', 'a.thumbnail', function (e) {
  	e.preventDefault();
  	$('#video-modal').modal('show');
  });

  equalHeight($(".eqHeight"));

});